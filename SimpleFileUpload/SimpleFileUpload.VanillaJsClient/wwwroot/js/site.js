﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
var ajax;

function uploadFile() {
    var file = $("#uploadFileInput")[0].files[0];

    var formdata = new FormData();
    formdata.append("file", file, file.name);
    ajax = new XMLHttpRequest();
    ajax.upload.addEventListener("progress", progressHandler, false);
    ajax.addEventListener("load", completeHandler, false);
    ajax.addEventListener("error", errorHandler, false);
    ajax.addEventListener("abort", abortHandler, false);
    ajax.open("PUT", "https://localhost:6001/file");
    ajax.send(formdata);
}

function progressHandler(event) {
    console.log('Progress: ', event);
    $("#loaded_n_total")[0].innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
    var percent = Math.round((event.loaded / event.total) * 100);
    $("#progressBar")[0].value = percent;
    $("#status")[0].innerHTML = percent + "% uploaded... please wait";
}

function completeHandler(event) {
    $("#status")[0].innerHTML = event.target.responseText;
    $("#progressBar")[0].value = 0; //wil clear progress bar after successful upload
}

function errorHandler(event) {
    $("#status")[0].innerHTML = "Upload Failed";
}

function abortHandler(event) {
    $("#status")[0].innerHTML = "Upload Aborted";
}

$(document).ready(function () {
    console.log('Ready!');
    $('#upload_form').on('submit', (e) => {
        e.preventDefault();
        uploadFile();
    });

    $('#cancel_button').on('click', (e) => {
        ajax.abort();
    });
});
