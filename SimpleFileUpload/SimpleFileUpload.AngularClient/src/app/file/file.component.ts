import { HttpClient, HttpEventType } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss']
})
export class FileComponent implements OnInit {

  progress = 0;
  @ViewChild('inputFile')
  inputFile!: ElementRef;

  @ViewChild('progressBar')
  progressBar!: ElementRef;

  uploadStatus: string = '';
  status: string = '';

  constructor(private http: HttpClient) { 

  }

  ngOnInit(): void {
  }

  sendFile() {
    let file: File = this.inputFile.nativeElement.files[0];
    let formData = new FormData();
    formData.append("file", file, "video.mov");

    this.http.put('https://localhost:6001/file', formData, {
      observe: 'events',
      reportProgress: true
    }).subscribe(event => {
      switch (event.type) {
        case HttpEventType.UploadProgress:
          console.log('Uploading....');
          let total = event.total ?? 0;
          let p = (event.loaded / total) * 100;
          this.progress = Math.round(p);
          (<HTMLProgressElement> this.progressBar.nativeElement).value = this.progress;
          this.uploadStatus = "Uploaded " + event.loaded + " bytes of " + event.total;
      break;
        case HttpEventType.Response:
          console.log('Response', event.status, event.body);
          this.status = JSON.stringify(event.body);
      break;
        default:
          //return `File "x" surprising upload event: ${event.type}.`;
      }
    })
  }
}
