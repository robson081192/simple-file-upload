﻿using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SimpleFileUpload.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FileController : ControllerBase
    {
        private readonly string _destinationPath = @"H:\Work\Gitlab\simple-file-upload\storage";

        [HttpPut]
        [DisableRequestSizeLimit]
        // see https://github.com/aspnet/HttpAbstractions/blob/87cd79d6fc54bb4abf07c1e380cd7a9498a78612/src/Microsoft.AspNetCore.Http/Features/FormOptions.cs#L76
        [RequestFormLimits(MultipartBodyLengthLimit = long.MaxValue)]
        public async Task<IActionResult> UploadFile([FromForm(Name = "file")] List<IFormFile> files)
        {
            foreach (var file in files)
            {
                var destination = Path.Combine(_destinationPath, Path.GetRandomFileName());
                await using (var fileStream = new FileStream(destination, FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
                {
                    await file.CopyToAsync(fileStream, CancellationToken.None);
                }
            }
            return Ok(new {files.Count});
        }
    }
}
